<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';

//filter by status
$status = isset($_REQUEST['status'])&& strlen($_REQUEST['status']) > 0?$_REQUEST['status']:null;

$room = isset($_REQUEST['room'])&& strlen($_REQUEST['room']) > 0?$_REQUEST['room']:null;


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "select  d.en, i.* from items i INNER JOIN dictionary d on d.id = i.titleId AND i.type = \"optionset\" order by d.en asc";

$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}


if($st->fetchColumn() > 0 || $st->rowCount() > 0){

    echo returnStatus(1, 'get list OK',$list);
}
else{
    echo returnStatus(0, 'get list fail',$list);
}


return 0;

?>
