<?php 

require( "../config.php" );
require("../php/inc.appvars.php");

$mediaId = $_POST['mediaId'];
$itemId= $_POST['itemId'];

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE mediaItemMap SET  prefer = 0 where itemId = :itemId AND prefer = 1";
$st = $conn->prepare ( $sql );
$st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
$st->execute();



$sql = "UPDATE mediaItemMap SET  prefer = 1, lastUpdateTime = now() where itemId = :itemId AND mediaId = :mediaId";
$st = $conn->prepare ( $sql );
$st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
$st->bindValue( ":mediaId", $mediaId   , PDO::PARAM_STR );
$st->execute();

echo($itemId);

$conn = null;

echo returnStatus(1 , 'good',array('mediaId' => $mediaId));
?>
