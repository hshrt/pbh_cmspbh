<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$roomNum = isset($_POST['roomNum'])?$_POST['roomNum']:'';
$regNum = isset($_POST['regNum'])?$_POST['regNum']:'';
$lastUpdateBy = isset($_POST['lastUpdateBy'])?$_POST['lastUpdateBy']:'';

if ( empty($roomNum)){
    echo returnStatus(0, 'missing room number');
    exit;
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "UPDATE guest SET status=:status ,lastUpdateBy=:lastUpdateBy where room = :room";

    $st = $conn->prepare ( $sql );

    $st->bindValue( ":room", $roomNum, PDO::PARAM_STR );
    //$st->bindValue( ":regNum", $regNum, PDO::PARAM_STR );
    $st->bindValue( ":status", 'out', PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy", $lastUpdateBy, PDO::PARAM_STR );

    //$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
    $st->execute();
    echo '$st->rowCount() = '.$st->rowCount();

    if($st->fetchColumn() > 0 || $st->rowCount() > 0)
        echo returnStatus(1 , 'checkout ok!');
    else
        echo returnStatus(0 , 'checkout fail! ');
}


?>
