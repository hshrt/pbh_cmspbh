<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$guestId = isset($_POST['guestId'])?$_POST['guestId']:null;


if ( empty($guestId)){
    echo returnStatus(0, 'missing guest Id');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "DELETE FROM guest where id = :id";

$st = $conn->prepare ( $sql );

$st->bindValue( ":id", $guestId, PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount() > 0)
    echo returnStatus(1 , 'delete ok!');
else
    echo returnStatus(0 , 'delete fail! ');

$conn = null;

?>
