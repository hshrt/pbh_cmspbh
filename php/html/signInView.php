<!-- popup box setup -->
<div id="signInContainer">
    <div class='popup_box signIn_box'>
        <div id="head">
        </div>

        <div id="middle">
            <h2>Sign In</h2>
            <input id="email" class='form-control' type='text' name='title' data-type='text' maxlength="50" placeholder="email"
                   autofocus=''>
            <input id="pwd" class='form-control' type='password' name='title' data-type='text' maxlength='25' placeholder="password"
                   autofocus=''>

            <div id="warning">   </div>

        </div>

        <div id="foot">
            <div class="actions">
                <div class="tips"></div>
                <div style='float:right' class='submit round_btn btn bg-olive' id="signInBtn">Sign In</div>
            </div>
        </div>
    </div>
</div>