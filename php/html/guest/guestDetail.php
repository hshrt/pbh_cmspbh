<div class='detail_container'>
    <div style='width:100%;height:auto;float:left'>
        <h1 id='itemName'></h1>

    </div>

    <div class='half_width_container' id= 'container_salutation'>
        <label for='title' id='salutationLabel'>Salutation</label>
        <input id='salutationInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>
    <div class='half_width_container' id= 'container_firstName'>
        <label for='title' id='firstNameLabel'>First Name</label>
        <input id='firstNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>
    <div class='half_width_container' id= 'container_lastName'>
        <label for='title' id='lastNameLabel'>Last Name</label>
        <input id='lastNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>
    <div class='half_width_container' id= 'container_roomNum'>
        <label for='title' id='roomNumLabel'>Room Number</label>
        <input id='roomNumInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>

    <div class='half_width_container' id= 'container_checkIn'>
        <div class='half_width_container_small' id= 'container_checkInDate'>
            <label for='title' id='checkInDateLabel'>Check In Date</label>
            <input id='checkInDateInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container_small half_width_container_small_right' id= 'container_checkInTime'>
            <label for='title' id='checkInTimeLabel'>Check In Time</label>
            <input id='checkInTimeInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
    </div>

    <div class='half_width_container' id= 'container_checkOut'>
        <div class='half_width_container_small' id= 'container_checkOutDate'>
            <label for='title' id='checkOutDateLabel'>Check Out Date</label>
            <input id='checkOutDateInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container_small half_width_container_small_right' id= 'container_checkOutTime'>
            <label for='title' id='checkOutTimeLabel'>Check Out Time</label>
            <input id='checkOutTimeInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
    </div>

    <div class='half_width_container' id= 'container_reserveId'>
        <label for='title' id='reserveLabel'>Reservation ID</label>
        <input id='reserveIdInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>

    <div class='half_width_container' id= 'container_memberId'>
        <label for='title' id='memberIdLabel'>MemberShip ID</label>
        <input id='memberIdInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
    </div>


    <div class='save_cancel_container'>
        <div class='round_btn' id='delete_btn'>Delete Section</div>
        <div class='round_btn' id='save_btn'>Save Section</div>
    </div>
</div>