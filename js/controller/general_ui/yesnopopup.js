App.YesNoPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    msg:"",
    yesFunc: function(){

    },
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.render();
        this.yesFunc = options.yesFunc;

        if(options && options.msg){
            this.msg = options.msg;
        }
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render YesNoPopup");
        $.ajax({
         url : "php/html/yesNoPopup.php",
         method : "POST",
         dataType: "html",
         data : { }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){
                    //$(this).setupUIHandler();

                    $('#yes_no_root').show();
                    $('#msg').text(self.msg);
                    $('#confirm_btn').on('click',function(){

                        self.yesFunc();
                        //self.destroy();


                    });
                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $("#yes_no_root").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});