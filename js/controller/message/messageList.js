App.MessageList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObj: null,
    jsonObj_filtered:null,
    sortType:'endDate',
    sortDirection: 'ASC',
    title: "Message",
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        if(options && options.type){
            this.type = options.type;
        }

        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){




        /*$(this.el).append(
        "<div id='itemListContainer'>"+
        "<div class='table-header'>"+
            "<h1>Messages</h1>"+
            "<div class='submit round_btn' id='addBtn'>+Add Message</div>"+
        "</div>"+
            "<table id='headerTable'>"+
            "<tr class='headRow2'>"+
            tableHeaderString+
            "</tr></table>"+

            "<table id='resultTable'>"+
            "<tr class='headRow'>"+
            tableHeaderString+
            "</tr></table>"+

        "</table></div>"

            );*/

        var self = this;

        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");
                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));
                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });



    },
    postUISetup:function(){

        var self = this;
        //set up the title
        $("#item_list_head_title").text(self.title);

        $('#addBtn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            //that.showPopup();
            window.location = document.URL+"/"+"new";
        });

        //setup add btn text
        $('#addBtn').text("+Add " + self.title);

        App.showLoading();

        var inputData = null;

        if(this.type == 'active'){ inputData = {getActive:true} };
        if(this.type == 'inactive'){ inputData = {getInactive:true} };

        $.ajax({
            url : "api/msg/getMessage.php",
            method : "GET",
            dataType: "json",
            data :inputData
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            App.hideLoading();

            console.log(json);
            App.messageList.jsonObj = json.data;
            self.drawMessage(App.messageList.jsonObj);

            $("#guestname_sort_icon").hide();
            $("#subject_sort_icon").hide();
            $("#expire_sort_icon").hide();
            $("#source_sort_icon").hide();
            $("#update_sort_icon").hide();
            $("#room_sort_icon").hide();

            console.log(App.idArray);

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });

        $('#search').on('keyup', function() {
            if (this.value.length > 0) {

                self.jsonObj_filtered = new Array();
                self.jsonObj_filtered = searchStringInSubject($("#search").val(),self.jsonObj);

                if(self.jsonObj_filtered.length == 0){
                    $("#resultTable").empty();
                    $("#noResultMsg").show();
                }
                else{
                    console.log("yeah");

                    $("#noResultMsg").hide();

                    self.filtering = true;

                    self.drawMessage(self.jsonObj_filtered);
                }
            }
            else{
                $("#paginationContainer").show();
                $("#noResultMsg").hide();
                self.filtering = false;
                self.drawMessage(self.jsonObj);
            }
        });
    },
    drawMessage: function(objects){

        var self = this;
        $("#resultTable").empty();

        //Insert Table Header
        var tableHeaderString = "<tr><th class='tableHead'><span id='nameText'>To</span><span class='glyphicon glyphicon-chevron-up' id ='guestname_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='roomText'>Room<span class='glyphicon glyphicon-chevron-up' id ='room_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='subjectText'>Subject<span class='glyphicon glyphicon-chevron-up' id ='subject_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='expireText'>Expires</span><span class='glyphicon glyphicon-chevron-up' id ='expire_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='sourceText'>Source</span><span class='glyphicon glyphicon-chevron-up' id ='source_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='updateText'>Last Update</span><span class='glyphicon glyphicon-chevron-up' id ='update_sort_icon'></span></th></tr>";

        $("#resultTable").append(tableHeaderString);

        $("#nameText").on('click',function(){

            self.sortButtonClick('name_list',"guestname_sort_icon");
        });

        $("#subjectText").on('click',function(){

            self.sortButtonClick('subject',"subject_sort_icon");
        });
        $("#expireText").on('click',function(){

            self.sortButtonClick('endDate',"expire_sort_icon");
        });
        $("#sourceText").on('click',function(){

            self.sortButtonClick('source',"source_sort_icon");
        });

        for(var y = 0; y<objects.length;y++){

            var nameList = "";
            if(objects[y].name_list!=null){
                nameList = objects[y].name_list;
            }
            else{
                nameList = "All guest";
            }

            var roomList =  +objects[y].room_list;
            if(objects[y].boardcast == 2){
                nameList = "All guest";
                roomList = "All";
            }

            var endDate = objects[y].endDate;

            if(endDate == "0000-00-00 00:00:00"){
                endDate = "N/A";
            }

            $("#resultTable").append(
                "<tr id='"+objects[y].id+"' style='height:30px'><td>"+"<div class ='titleContainer'></div>"+"<div class='titleText'>"+nameList+ "</div></div>"+"</td>"+
                "<td>"+roomList+"</td>"+
                "<td>"+objects[y].subject+"</td>"+
                "<td>"+endDate.substr(0,10)+"</td>" + "<td>"+objects[y].source+"</td>" +
                "<td>"+objects[y].lastUpdate+"</td>" +
                "</tr>");

            $("#"+objects[y].id).attr( "index", y );

            /*if(objects[y].source == "HOTEL"){
                $("#"+objects[y].id).find('*').css({cursor:"default"});
            }*/

            (function (jsonitem) {
                //alert(Area);
                $("#"+jsonitem.id).on('click',function(){
                    //alert($(this).attr('index'));
                    //alert(document.URL);

                    /*if(jsonitem.source == "HOTEL"){
                        return;
                    }*/

                    App.currentId = $(this).attr('id');
                    App.currentItem = jsonitem;
                    //alert(jsonObj.result[y].id);
                    window.location = document.URL+"/"+$(this).attr('id');
                });
            })(objects[y]);

            $("#nameText,#subjectText,#expireText,#sourceText").css({cursor:"pointer"});
        }
    },
    sortButtonClick : function(_sortType,sortIcon){
        //alert("Yeah");
        console.log("hide all");
        $("#guestname_sort_icon").hide();
        $("#subject_sort_icon").hide();
        $("#expire_sort_icon").hide();
        $("#source_sort_icon").hide();

        $("#"+sortIcon).show();

        if(this.sortType == _sortType){
            if(this.sortDirection == 'ASC'){
                this.sortBy(_sortType,'DESC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-up");
                $("#"+sortIcon).addClass("glyphicon-chevron-down");
            }
            else {
                this.sortBy(_sortType, 'ASC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-down");
                $("#"+sortIcon).addClass("glyphicon-chevron-up");
            }
        }
        else{
            this.sortType = _sortType;
            this.sortBy(_sortType, 'ASC');
            $("#"+sortIcon).removeClass("glyphicon-chevron-down");
            $("#"+sortIcon).addClass("glyphicon-chevron-up");
        }

        $("#guestname_sort_icon").hide();
        $("#subject_sort_icon").hide();
        $("#expire_sort_icon").hide();
        $("#source_sort_icon").hide();

        $("#"+sortIcon).show();

    },
    sortBy: function(type,order){
        console.log("order = " + order);
        this.sortDirection = order;
        if(type == "name_list"){
            if(order == 'ASC') {
                function SortByName(a, b){
                    var aName = a.name_list.toLowerCase();
                    var bName = b.name_list.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = b.name_list.toLowerCase();
                    var bName = a.name_list.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByNameRev);
            }
        }
        else if(type == "subject"){
            if(order == 'ASC') {

                function SortByName(a, b){
                    var aName = a.subject.toLowerCase();
                    var bName = b.subject.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = b.subject.toLowerCase();
                    var bName = a.subject.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByNameRev);
            }
        }
        else if(type == "endDate"){
            if(order == 'ASC') {
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj1.endDate - obj2.endDate;
                });
            }
            else{
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj2.endDate - obj1.endDate;
                });
            }
        }
        else if(type == "source"){
            if(order == 'ASC') {
                function SortByName(a, b){
                    var aName = a.source.toLowerCase();
                    var bName = b.source.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = b.source.toLowerCase();
                    var bName = a.source.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByNameRev);
            }
        }

        this.drawMessage(this.jsonObj);
    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});